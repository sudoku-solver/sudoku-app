const board = new Array(9);
for (let i = 0; i < 9; i++) {
    board[i] = new Array(9);
}

// validate cell input. Only allow digits 1-9 and Backspace for deletion
function validateCellInput(evt, element) {
    // remove animation classes
    element.classList.remove("rotate-scale-down", "rotate-scale-down-reverse");

    // Handle paste
    let key;
    if (evt.type === 'paste') {
        key = evt.clipboardData.getData('text/plain');
    } else {
        // Handle key press
        key = evt.keyCode || evt.which;
        key = String.fromCharCode(key);
    }

    // get row and column of current cell
    const rowColumn = element.id.split("__");

    // if backspace, reset cell and board state
    if (evt.keyCode === 8) {
        if (rowColumn.length === 2) {
            board[parseInt(rowColumn[0])][parseInt(rowColumn[1])] = 0;
        }
        evt.returnValue = true;
        return;
    }

    const regex = /[1-9]|\./;
    if (!regex.test(key)) {
        evt.returnValue = false;
        if (evt.preventDefault) evt.preventDefault();
    } else {
        if (element.value.length > 0) {
            evt.returnValue = false;
            if (evt.preventDefault) evt.preventDefault();
            return;
        }

        // if cell and input are valid, update board state
        if (rowColumn.length === 2) {
            board[parseInt(rowColumn[0])][parseInt(rowColumn[1])] = parseInt(evt.key);
        }
    }
}

// initialize the web socket connection and add event listeners
function initWebSocket() {
    ws.onopen = function () {
    };
    ws.onclose = function () {
        ws.close();
    };
    ws.onmessage = function (evt) {
        processMessage(evt.data);
    };
    ws.onerror = function (evt) {
    };
}

// process an incoming message via web socket
function processMessage(message) {
    const data = JSON.parse(message);

    // if the board is completed, prepare the 2-dim board array
    if (data['updateType'] === 'BoardReady') {
        handleBoardReady();
    } else {
        // if message is not important, skip processing
        if (data['cell'] && data['cell']['row'] === 0 && data['cell']['column'] === 0 && data['cell']['value'] === 0) {
            return
        }

        handleCellUpdate();
    }

    function handleBoardReady() {
        // update the board state
        for (let row = 0; row < 9; row++) {
            for (let column = 0; column < 9; column++) {
                const d = document.getElementById(row + "__" + column);
                if (d.value !== '') {
                    board[row][column] = parseInt(d.value);
                } else {
                    board[row][column] = 0;
                }
            }
        }

        // show the ready animation
        const d = document.getElementById("animations-container");
        d.classList.remove("lds-hourglass");
        d.classList.add("ready-animation");
        d.innerHTML = "Ready!";
        setTimeout(() => {
            d.classList.remove("ready-animation");
            d.innerHTML = '';

            const button = document.getElementById("create");
            button.style.backgroundColor = '#4CAF50';
            button.removeAttribute("disabled");
        }, 5000);
    }

    function handleCellUpdate() {
        const d = document.getElementById(data['cell']['row'] + "__" + data['cell']['column']);

        // animate the cell depending on the update type
        if (data['updateType'] === 'CellFilled') {
            d.value = data['cell']['value'];
            d.setAttribute("disabled", "true");
            d.style.background = "#2dccb8";

            d.classList.remove("rotate-scale-down-reverse");
            d.classList.add("rotate-scale-down");
        } else if (data['updateType'] === 'CellReset') {
            d.value = '';
            d.removeAttribute("disabled");
            d.style.background = "#2dcc00";

            d.classList.remove("rotate-scale-down");
            d.classList.add("rotate-scale-down-reverse");
        }
    }
}

// Bring the board back to an empty state and remove all animation classes
function resetBoard() {
    for (let row = 0; row < 9; row++) {
        for (let column = 0; column < 9; column++) {
            const d = document.getElementById(row + "__" + column);
            d.classList.remove("rotate-scale-down", "rotate-scale-down-reverse");
            d.style.background = "#2dccb8";
            d.value = '';
            board[row][column] = 0;
        }
    }
}

// create message which is sent via web socket to the backend
function createMessage(type) {
    const message = {};
    if (type === 'create') {
        message['type'] = 'create';
        const d = document.getElementById("difficulty-selector");
        const difficulty = d.options[d.selectedIndex].value;
        message['difficulty'] = parseInt(difficulty);
    }

    return JSON.stringify(message);
}

window.addEventListener("load", function () {
    resetBoard();
    initWebSocket();

    // send a create message to the backend and activates the 'in progress' animation
    document.getElementById("create").onclick = function () {
        resetBoard();

        const d = document.getElementById("animations-container");
        d.classList.add("lds-hourglass");

        const button = document.getElementById("create");
        button.style.backgroundColor = 'grey';
        button.setAttribute("disabled", "true");

        ws.send(createMessage("create"));
    };

    // sends a validation request to the backend and colors the button depending on the result
    document.getElementById("validate").onclick = function () {
        const request = new XMLHttpRequest();
        const theUrl = "/api/sudoku/validate";

        const payload = {};
        payload['board'] = board;
        request.open("POST", theUrl);
        request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        request.send(JSON.stringify(payload));
        request.onreadystatechange = function () {
            if (request.readyState === 4 && request.status === 200) {
                const response = JSON.parse(request.responseText);
                const d = document.getElementById("validate");
                if (!response['valid']) {
                    d.style.backgroundColor = 'red';
                    setTimeout(() => d.style.backgroundColor = '#779ecb', 1000)
                } else {
                    d.style.backgroundColor = "#4CAF50";
                    setTimeout(() => d.style.backgroundColor = '#779ecb', 1000)
                    if (response['solved']) {
                        document.getElementById("sudoku").style.display = 'none';
                        document.getElementById("pyro-wrapper").style.display = 'block';
                        document.getElementById("pyro-wrapper").classList.add("pyro");
                        document.getElementById("pyro-before").classList.add("pyro-before");
                        document.getElementById("pyro-after").classList.add("pyro-after");

                        setTimeout(() => {
                            document.getElementById("sudoku").style.display = 'grid';
                            document.getElementById("pyro-wrapper").style.display = 'none';
                            document.getElementById("pyro-wrapper").classList.remove("pyro");
                            document.getElementById("pyro-before").classList.remove("pyro-before");
                            document.getElementById("pyro-after").classList.remove("pyro-after");
                            resetBoard();
                        }, 10000);
                    }
                }
            }
        };
    };

    // reset the current board state to the initial board state
    document.getElementById("reset").onclick = function () {
        for (let row = 0; row < 9; row++) {
            for (let column = 0; column < 9; column++) {
                const d = document.getElementById(row + "__" + column);
                d.classList.remove("rotate-scale-down", "rotate-scale-down-reverse");
                if (!d.attributes.getNamedItem("disabled")) {
                    if (d.value !== '') {
                        d.classList.add("rotate-scale-down-reverse");
                    }

                    d.value = '';
                    board[row][column] = 0;
                }
            }
        }
    };
});
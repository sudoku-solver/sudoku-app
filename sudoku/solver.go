package sudoku

import (
	"errors"
	"math/rand"
)

// UpdateMessage holds information for a cell update, which can be sent to the frontend for rendering
type UpdateMessage struct {
	UpdateType UpdateType `json:"updateType"`
	Cell       Cell       `json:"cell"`
}

type UpdateType string

const (
	CellFilled UpdateType = "CellFilled"
	CellReset  UpdateType = "CellReset"
	BoardReady UpdateType = "BoardReady"
)

// CreateSudoku creates a sudoku board according to the given difficulty.
func CreateSudoku(difficulty int, channel chan UpdateMessage) (Sudoku, error) {
	sudoku, err := Init([9][9]int{})

	if err != nil {
		return sudoku, err
	}

	_, err = Solve(&sudoku, channel, true)

	if err != nil {
		return sudoku, err
	}

	prepareBoard(&sudoku, difficulty, channel)

	return sudoku, nil
}

// Solve solves a sudoku board in a given state
func Solve(sudoku *Sudoku, channel chan UpdateMessage, sendToChannel bool) (*Sudoku, error) {
	// first validate, that board is in a valid state
	if !sudoku.BoardValid() {
		return sudoku, errors.New("sudoku board is not valid")
	}

	Backtrack(sudoku, channel, sendToChannel)

	return sudoku, nil
}

// AreEqual compares two sudoku boards and returns true, if all cells are of equal values.
func AreEqual(first Sudoku, second Sudoku) bool {
	for row := 0; row < 9; row++ {
		for column := 0; column < 9; column++ {
			if first.Board[row][column].Value != second.Board[row][column].Value {
				return false
			}
		}
	}
	return true
}

func prepareBoard(sudoku *Sudoku, difficulty int, channel chan UpdateMessage) {
	cellsToRemove := difficulty

	filledCells := filledCells(sudoku)

	maxMisses := 5
	misses := 0
	for {
		if cellsToRemove <= 0 || misses == maxMisses {
			break
		}

		cell, i := determineCellToRemove(filledCells)

		sudoku.Board[cell.Row][cell.Column].Value = 0

		uniquelySolvable := boardUniquelySolvable(sudoku)

		if !uniquelySolvable {
			misses++
			sudoku.Board[cell.Row][cell.Column].Value = cell.Value
			continue
		}

		sudoku.Board[cell.Row][cell.Column].Filled = false
		filledCells = removeCell(filledCells, i)
		misses = 0
		cellsToRemove--
		channel <- UpdateMessage{UpdateType: CellReset, Cell: Cell{Row: cell.Row, Column: cell.Column, Value: 0}}
	}
}

func removeCell(cells []Cell, i int) []Cell {
	cells[len(cells)-1], cells[i] = cells[i], cells[len(cells)-1]
	return cells[:len(cells)-1]
}

func filledCells(sudoku *Sudoku) []Cell {
	var cells []Cell
	for row := 0; row < 9; row++ {
		for column := 0; column < 9; column++ {
			cells = append(cells, *sudoku.Board[row][column])
		}
	}

	return cells
}

func boardUniquelySolvable(sudoku *Sudoku) bool {
	input := sudoku.ConvertBoard()
	var sudokus [5]Sudoku
	for i := 0; i < 5; i++ {
		s, _ := Init(input)
		sudokus[i] = s
		_, _ = Solve(&s, nil, false)

		if i > 0 && !AreEqual(sudokus[i-1], sudokus[i]) {
			return false
		}
	}

	return true
}

func determineCellToRemove(cells []Cell) (Cell, int) {
	row := rand.Intn(len(cells))
	return cells[row], row
}

package sudoku

import (
	"math/rand"
	"time"
)

// Backtrack implements the Backtrack algorithm for solving a sudoku board
func Backtrack(sudoku *Sudoku, channel chan UpdateMessage, sendToChannel bool) bool {
	board := sudoku.Board

	// if the board is solved, we are done
	if boardSolved(board) {
		return true
	}

	// as we want random sudoku boards, we shuffle the values before testing them
	rowIndices := []int{0, 1, 2, 3, 4, 5, 6, 7, 8}
	columnIndices := []int{0, 1, 2, 3, 4, 5, 6, 7, 8}

	for _, row := range rowIndices {
		for _, column := range columnIndices {
			cell := board[row][column]

			// only empty cells should be considered
			if cell.Value == 0 {
				// as we want random sudoku boards, we shuffle the values before testing them
				values := shuffleInts([]int{1, 2, 3, 4, 5, 6, 7, 8, 9})

				// try out values for the given cell as long as the board is valid
				// if not, reset the value to 0 and Backtrack
				for _, consideredValue := range values {
					cell.Value = consideredValue

					if sendToChannel {
						channel <- UpdateMessage{UpdateType: CellFilled, Cell: Cell{Row: cell.Row, Column: cell.Column, Value: consideredValue}}
					}
					if sudoku.BoardValid() {
						if Backtrack(sudoku, channel, sendToChannel) {
							return true
						}
					}
					cell.Value = 0

					if sendToChannel {
						channel <- UpdateMessage{UpdateType: CellReset, Cell: Cell{Row: cell.Row, Column: cell.Column, Value: 0}}
					}
				}
				return false
			}
		}
	}
	return false
}

func boardSolved(board [9][9]*Cell) bool {
	// a board is solved, if no empty cells are remaining
	for row := 0; row < 9; row++ {
		for column := 0; column < 9; column++ {
			if board[row][column].Value == 0 {
				return false
			}
		}
	}
	return true
}

func shuffleInts(values []int) []int {
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(values), func(i, j int) { values[i], values[j] = values[j], values[i] })

	return values
}

package sudoku

import (
	"errors"
	"fmt"
)

// SuperCellIndices defines the corner indices of a super cell
type SuperCellIndices struct {
	rowStart    int // upper left corner
	rowEnd      int // upper right corner
	columnStart int // bottom left corner
	columnEnd   int // bottom right corner
}

// Init initializes a sudoku board with the given input
func Init(input [9][9]int) (Sudoku, error) {
	sudoku := Sudoku{}
	if !inputIsValid(input) {
		return sudoku, errors.New("input values have to be between 0 and 9, where zero means unassigned")
	}

	sudoku.input = input

	// init board
	board := createBoard(input)
	sudoku.Board = board

	// init rows
	rows, err := createRows(sudoku.Board)
	if err != nil {
		return sudoku, err
	}
	sudoku.Rows = rows

	//init columns
	columns, err := createColumns(sudoku.Board)
	if err != nil {
		return sudoku, err
	}
	sudoku.Columns = columns

	// init super cells
	superCells, err := createSuperCells(sudoku.Board)
	if err != nil {
		return sudoku, err
	}
	sudoku.SuperCells = superCells

	sudoku.Initialized = true

	return sudoku, nil
}

func createBoard(input [9][9]int) [9][9]*Cell {
	var board [9][9]*Cell
	for i := 0; i < 9; i++ {
		for j := 0; j < 9; j++ {
			board[i][j] = &Cell{Row: i, Column: j, Value: input[i][j], Filled: input[i][j] != 0}
		}
	}

	return board
}

func createRows(board [9][9]*Cell) ([9]Row, error) {
	var rows [9]Row
	for index, rowList := range board {
		var rowValues [9]*Cell
		for j := 0; j < len(rowValues); j++ {
			rowValues[j] = rowList[j]
		}
		row, err := createRow(rowValues)
		if err != nil {
			return rows, err
		}
		rows[index] = row
	}

	return rows, nil
}

func createColumns(board [9][9]*Cell) ([9]Column, error) {
	var columns [9]Column
	for columnIndex := 0; columnIndex < 9; columnIndex++ {
		var columnValues [9]*Cell
		for index, rowValues := range board {
			columnValues[index] = rowValues[columnIndex]
		}

		column, err := createColumn(columnValues)
		if err != nil {
			return columns, err
		}
		columns[columnIndex] = column
	}

	return columns, nil
}

func createSuperCells(board [9][9]*Cell) ([9]SuperCell, error) {
	var superCells [9]SuperCell

	superCellParts := []SuperCellIndices{
		{rowStart: 0, rowEnd: 2, columnStart: 0, columnEnd: 2},
		{rowStart: 0, rowEnd: 2, columnStart: 3, columnEnd: 5},
		{rowStart: 0, rowEnd: 2, columnStart: 6, columnEnd: 8},
		{rowStart: 3, rowEnd: 5, columnStart: 0, columnEnd: 2},
		{rowStart: 3, rowEnd: 5, columnStart: 3, columnEnd: 5},
		{rowStart: 3, rowEnd: 5, columnStart: 6, columnEnd: 8},
		{rowStart: 6, rowEnd: 8, columnStart: 0, columnEnd: 2},
		{rowStart: 6, rowEnd: 8, columnStart: 3, columnEnd: 5},
		{rowStart: 6, rowEnd: 8, columnStart: 6, columnEnd: 8},
	}

	for index, part := range superCellParts {
		var superCellValues [9]*Cell
		superCellValuesIndex := 0
		for i := part.rowStart; i <= part.rowEnd; i++ {
			for j := part.columnStart; j <= part.columnEnd; j++ {
				superCellValues[superCellValuesIndex] = board[i][j]
				superCellValuesIndex++
			}
		}

		superCell, err := createSuperCell(superCellValues)
		if err != nil {
			return superCells, err
		}
		superCells[index] = superCell
	}

	return superCells, nil
}

func inputIsValid(input [9][9]int) bool {
	for i := 0; i < 9; i++ {
		for j := 0; j < 9; j++ {
			if input[i][j] < 0 || input[i][j] > 9 {
				return false
			}
		}
	}

	return true
}

func createRow(cells [9]*Cell) (Row, error) {
	valid := cellsValid(cells)

	if !valid {
		return Row{}, fmt.Errorf("row for input %v is not valid", cells)
	}

	return Row{Values: cells, valid: valid}, nil
}

func createColumn(cells [9]*Cell) (Column, error) {
	valid := cellsValid(cells)

	if !valid {
		return Column{}, fmt.Errorf("column for input %v is not valid", cells)
	}

	return Column{Values: cells, valid: valid}, nil
}

func createSuperCell(cells [9]*Cell) (SuperCell, error) {
	valid := cellsValid(cells)

	if !valid {
		return SuperCell{}, fmt.Errorf("super cell for input %v is not valid", cells)
	}

	return SuperCell{Values: cells, valid: valid}, nil
}

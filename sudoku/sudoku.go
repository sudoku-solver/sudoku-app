// Package sudoku contains functions to initialize, validate and solve sudoku boards
package sudoku

import (
	"fmt"
	"strings"
)

// a Cell describes one cell in on a sudoku board. It can have a value from 1 to 9 or it can be empty.
type Cell struct {
	Row    int  `json:"row"`    // the row index of the cell
	Column int  `json:"column"` // the column index of the cell
	Value  int  `json:"value"`  // the value of the cell
	Filled bool `json:"filled"` // true, if value is from 1 to 9
}

// a Row is one row on a sudoku board. It contains 9 values and is valid, if all values from 1 to 9 occur exactly once.
type Row struct {
	Values [9]*Cell `json:"values"` // a list of the cells in the row
	valid  bool     // true, if row is valid
}

// a Column is one row on a sudoku board. It contains 9 values and is valid, if all values from 1 to 9 occur exactly once.
type Column struct {
	Values [9]*Cell `json:"values"` // a list of the cells in the column
	valid  bool     // true, if column is valid
}

// a SuperCell is one big 3x3 cell on a sudoku board. It contains 9 values and is valid, if all values from 1 to 9 occur exactly once.
type SuperCell struct {
	Values [9]*Cell `json:"values"` // a list of the cells in the super cell
	valid  bool     // true, if the super cell is valid
}

// String pretty prints a super cell
func (s SuperCell) String() string {
	var sb strings.Builder

	sb.WriteString("[")
	for i := 0; i < len(s.Values); i++ {
		sb.WriteString(fmt.Sprintf("%d, ", s.Values[i].Value))

		if i == len(s.Values)-1 {
			sb.WriteString("]\n")
		}
	}
	return sb.String()
}

// a Sudoku is a sudoku board configuration
type Sudoku struct {
	input       [9][9]int    // the input values
	Board       [9][9]*Cell  `json:"board"` // the current board state
	Rows        [9]Row       // the rows of the sudoku board
	Columns     [9]Column    // the columns of the sudoku board
	SuperCells  [9]SuperCell // the super cells of the sudoku board
	Initialized bool         // true, if sudoku is initialized and valid
}

// PrintInput pretty prints the input sudoku board
func (s Sudoku) PrintInput() string {
	var sb strings.Builder

	for i := 0; i < 9; i++ {
		sb.WriteString("[")
		for j := 0; j < 9; j++ {
			if j < 8 {
				sb.WriteString(fmt.Sprintf("%d, ", s.input[i][j]))
			} else {
				sb.WriteString(fmt.Sprintf("%d]\n", s.input[i][j]))
			}
		}
	}
	return sb.String()
}

// PrintRows pretty prints the rows of the sudoku board
func (s Sudoku) PrintRows() string {
	var sb strings.Builder

	for i := 0; i < len(s.Rows); i++ {
		sb.WriteString("[")
		for j := 0; j < len(s.Rows[i].Values); j++ {
			if j < len(s.Rows[i].Values)-1 {
				sb.WriteString(fmt.Sprintf("%d, ", s.Rows[i].Values[j].Value))
			} else {
				sb.WriteString(fmt.Sprintf("%d]\n", s.Rows[i].Values[j].Value))
			}
		}
	}
	return sb.String()
}

// PrintColumns pretty prints the columns of the sudoku board
func (s Sudoku) PrintColumns() string {
	var sb strings.Builder

	for i := 0; i < len(s.Columns); i++ {
		sb.WriteString("[")
		for j := 0; j < len(s.Columns[i].Values); j++ {
			if j < len(s.Columns[i].Values)-1 {
				sb.WriteString(fmt.Sprintf("%d, ", s.Columns[j].Values[i].Value))
			} else {
				sb.WriteString(fmt.Sprintf("%d]\n", s.Columns[j].Values[i].Value))
			}
		}
	}
	return sb.String()
}

// PrintSuperCells pretty prints the super cells of the sudoku board
func (s Sudoku) PrintSuperCells() string {
	var sb strings.Builder

	for i := 0; i < 3; i++ {
		for j := 0; j < 3; j++ {
			sb.WriteString("[")
			for k := 0; k < 3; k++ {
				sb.WriteString(fmt.Sprintf("%d", s.SuperCells[j].Values[(i*3)+k].Value))
				if k < 2 {
					sb.WriteString(", ")
				}
			}
			sb.WriteString("] ")
		}

		sb.WriteString("\n")
	}

	sb.WriteString("-----------------------------\n")

	for i := 0; i < 3; i++ {
		for j := 0; j < 3; j++ {
			sb.WriteString("[")
			for k := 0; k < 3; k++ {
				sb.WriteString(fmt.Sprintf("%d", s.SuperCells[3+j].Values[(i*3)+k].Value))
				if k < 2 {
					sb.WriteString(", ")
				}
			}
			sb.WriteString("] ")
		}

		sb.WriteString("\n")
	}

	sb.WriteString("-----------------------------\n")

	for i := 0; i < 3; i++ {
		for j := 0; j < 3; j++ {
			sb.WriteString("[")
			for k := 0; k < 3; k++ {
				sb.WriteString(fmt.Sprintf("%d", s.SuperCells[6+j].Values[(i*3)+k].Value))
				if k < 2 {
					sb.WriteString(", ")
				}
			}
			sb.WriteString("] ")
		}

		sb.WriteString("\n")
	}

	return sb.String()
}

// ConvertBoard returns the sudoku board as a 2-dimensional array of integers
func (s Sudoku) ConvertBoard() [9][9]int {
	var result [9][9]int
	for row := 0; row < 9; row++ {
		for column := 0; column < 9; column++ {
			result[row][column] = s.Board[row][column].Value
		}
	}

	return result
}

// BoardValid determines for a given sudoku, if its current board state is valid
func (s Sudoku) BoardValid() bool {
	// for the board to be valid, all rows, columns and super cells must be valid
	for index := 0; index < 9; index++ {
		if !cellsValid(s.Rows[index].Values) || !cellsValid(s.Columns[index].Values) || !cellsValid(s.SuperCells[index].Values) {
			return false
		}
	}
	return true
}

// BoardSolved determines, if the current board state is a valid and solved state
func (s Sudoku) BoardSolved() bool {
	valid := s.BoardValid()
	if !valid {
		return false
	}

	for row := 0; row < 9; row++ {
		for column := 0; column < 9; column++ {
			if s.Board[row][column].Value == 0 {
				return false
			}
		}
	}

	return true

}

// cellsValid determines for a given set of 9 cells, if they are valid
func cellsValid(cells [9]*Cell) bool {
	// map of the valid inputs, which are allowed to occur at most once
	validValues := map[int]bool{
		1: false,
		2: false,
		3: false,
		4: false,
		5: false,
		6: false,
		7: false,
		8: false,
		9: false,
	}

	for _, cell := range cells {
		if cell.Value == 0 {
			continue
		}

		// if value of the cell occurred already, the input cells are invalid
		if validValues[cell.Value] {
			return false
		}

		validValues[cell.Value] = true
	}

	return true
}

package controller

import (
	"html/template"
	"net/http"
)

type FrontendData struct {
	WsUrl   string
	Rows    []int
	Columns []int
}

// SudokuTemplate returns the template for the sudoku page
func SudokuTemplate(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.ParseFiles("resources/templates/sudoku.html"))

	data := FrontendData{WsUrl: "ws://" + r.Host + "/ws", Rows: []int{0, 1, 2, 3, 4, 5, 6, 7, 8}, Columns: []int{0, 1, 2, 3, 4, 5, 6, 7, 8}}
	err := tmpl.Execute(w, data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

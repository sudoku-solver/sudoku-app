package controller

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"io/ioutil"
	"log"
	"net/http"
	"sudoku-solver/sudoku"
	"sync"
	"time"
)

// SudokuValidationInput holds the board sent for validation
type SudokuValidationInput struct {
	Board [9][9]int `json:"board"`
}

// SudokuValidationOutput holds information about a sudoku boards validity.
// It is returned to the frontend für rendering and/or animation triggering.
type SudokuValidationOutput struct {
	Valid  bool `json:"valid"`
	Solved bool `json:"solved"`
}

// ValidateSudokuHandler receives a sudoku board and determines, if it is valid and solved
func ValidateSudokuHandler(w http.ResponseWriter, r *http.Request) {

	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var input SudokuValidationInput
	err = json.Unmarshal(b, &input)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	response := SudokuValidationOutput{Valid: true}

	s, err := sudoku.Init(input.Board)
	if err != nil {
		response.Valid = false
	} else {
		response.Solved = s.BoardSolved()
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	err = json.NewEncoder(w).Encode(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// WsMessage holds information for the creation of a sudoku board
type WsMessage struct {
	Type       string `json:"type"`
	Difficulty int    `json:"difficulty"`
}

var upgrader = websocket.Upgrader{}

// CreateSudokuWebSocket is the web socket connection which sends information for updated cells during sudoku board creation.
func CreateSudokuWebSocket(w http.ResponseWriter, r *http.Request) {
	var channel = make(chan sudoku.UpdateMessage)

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println("Could not upgrade web socket:", err)
		return
	}

	for {
		var msg WsMessage
		err := conn.ReadJSON(&msg)
		if err != nil {
			log.Println("Closing web socket connection: " + err.Error())
			err := conn.Close()
			if err != nil {
				log.Println("Close web socket connection error: " + err.Error())
			}
			return
		}

		start(channel, conn, msg)
	}
}

func start(channelUpdateMessages chan sudoku.UpdateMessage, conn *websocket.Conn, message WsMessage) {
	channelDone := make(chan bool)

	go createSudokuBoard(channelUpdateMessages, channelDone, message)

	done := false

	// wait for the sudoku board to be done and write the messages for updated cells to the web socket connection
	for {
		go func() {
			done = <-channelDone
		}()

		if done {
			break
		}

		updateMessage := <-channelUpdateMessages

		var network bytes.Buffer
		err := json.NewEncoder(&network).Encode(updateMessage)
		err = conn.WriteMessage(1, network.Bytes())
		if err != nil {
			log.Println("write error:", err)
			break
		}

		// wait before processing the next message for prettier animations on frontend
		time.Sleep(1 * time.Millisecond)
	}
}

func createSudokuBoard(channel chan sudoku.UpdateMessage, channelDone chan bool, message WsMessage) {
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		_, err := sudoku.CreateSudoku(message.Difficulty, channel)
		if err != nil {
			fmt.Printf("an error has occured: %s", err.Error())
			return
		}

		// board is ready
		channel <- sudoku.UpdateMessage{UpdateType: sudoku.BoardReady}

		wg.Done()

		channelDone <- true
	}()

	wg.Wait()
}

package web

import (
	"github.com/gorilla/mux"
	"net/http"
	"sudoku-solver/web/controller"
)

// InitRouter initializes the router for the app
func InitRouter() *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/api/sudoku/validate", controller.ValidateSudokuHandler).Methods("POST")
	r.HandleFunc("/sudoku", controller.SudokuTemplate).Methods("GET")
	r.HandleFunc("/ws", controller.CreateSudokuWebSocket)
	http.Handle("/", r)

	return r
}

package main

import (
	"fmt"
	"net/http"
	"sudoku-solver/web"
)

func main() {
	web.InitRouter()
	fs := http.FileServer(http.Dir("./resources"))
	http.Handle("/resources/", http.StripPrefix("/resources/", fs))
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		fmt.Println(err.Error())
	}
}

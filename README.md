# Sudoku Web Application
<img src="https://i.imgur.com/Y6WiRMb.png" width="1000px" height="500px">

## Introduction
This app lets you play Sudoku in the browser. Each game is generated at runtime for a given difficulty from 'Very easy' to 'Very hard'. The current board state can be also validated (button turns green for valid or red for invalid) or reset to the starting state. If the board is successfully solved, the validation triggers happy fireworks and the game ends.

## Docker
A docker image for the app can be found on: [https://hub.docker.com/r/devenv571/sudoku-app](https://hub.docker.com/r/devenv571/sudoku-app)

## Installation

### From Source
The executable can be build with the file <code>/cmd/web/main.go</code>. The resources folder should be in the same directory as the executable at runtime.

### Docker Image
<pre>
<code>
$ docker run -p 8080:8080 devenv571/sudoku-app:latest
</code>
</pre>

After installation, the app is reachable on <code>http://localhost:8080/sudoku</code>.

### Helm Chart

<pre>
<code>
$ helm install sudoku-app . // inside helm-chart directory
</code>
</pre>

After installation, the app is reachable on <code>http://ClusterIP:30001/sudoku</code>.
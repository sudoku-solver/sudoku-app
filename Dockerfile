# build stage
FROM golang:alpine AS build-env
RUN apk --no-cache add build-base git bzr mercurial gcc
ADD . /src
RUN cd /src/cmd/web && CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o sudoku-solver

# final stage
FROM scratch
WORKDIR /app
COPY --from=build-env /src/cmd/web/sudoku-solver /app/sudoku-solver
COPY --from=build-env /src/resources /app/resources

EXPOSE 8080

ENTRYPOINT ["/app/sudoku-solver"]
